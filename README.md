# JavaFX aplikacija za testiranje algoritama sortiranja

JavaFX GUI aplikacija za testiranje algoritama sortiranja podijeljena je na dva osnovna dijela:
* Odabir algoritama sortiranja
* Generiranje i testiranje uzoraka
Početni prozor nakon pokretanja aplikacije prikazan je na Slici 1.

![alt text](https://imgur.com/Bh86Wsp.jpg "Slika 1.") <br>
Slika 1. Prikaz početnog prozora aplikacije 
### Odabir algoritama sortiranja
Korisnik odabire algoritme sortiranja za koje želi izvršiti testiranja (Slika 2.). 

![alt text](https://imgur.com/eEfUbbL.jpg "Slika 2.") <br>
Slika 4.5. Prikaz postupka odabira algoritama sortiranja za testiranje <br>

Kako bi odabrali željene algoritme potrebno je klikom miša odabrati padajući izbornik „Odabir  algoritama“ te staviti kvačicu pored algoritma kojeg želimo testirati. Odabrani algoritmi se automatski prikazuju na popisu odabranih algoritama.

### Generiranje i testiranje uzoraka
Stavljanjem kvačice pored teksta „Uključi granice“ korisnik ima mogućnost unosa raspona unutar kojeg želi generiranje slučajnih cjelobrojnih brojeva (Slika 3.). <br>
* Ograničenja:
** Donja (gornja) granica ne može biti veća od 10 000
** Donja granica ne može biti veća od gornje
** Granice ne mogu biti negativne
* Ukoliko granice nisu uključene generiraju se slučajni brojevi od 0 – 5 000.
 
![alt text](https://imgur.com/ZF7CceA.jpg "Slika 3.") <br>
Slika 3. Postupak uključivanja i podešavanja granica <br>

Korisnik u prostor označen na Slici 4. unosi veličinu polja te pritiskom na tipku „Dodaj“ dodaje novo polje koje se zatim prikazuje na popisu uzoraka.

![alt text](https://imgur.com/Fabk0Qj.jpg "Slika 4.") <br>
Slika 4. Prikaz postupka dodavanja uzorka
* Pojašnjenje gumbova za manipuliranjem uzorcima:
** Dodaj – koristi se za dodavanje novog polja željene veličine na popis uzoraka
** Izmijeni – koristi se za izmjenu veličine odabranog uzorka
** Obriši – koristi se za brisanje odabranog uzorka
** Očisti – koristi se za brisanje svih uzoraka s popisa
* Ograničenja:
** Veličina uzorka ne može biti veća od 1 000 000
** Korisnik ne može dodati više od 5 uzoraka
** Korisnik ne može unijeti negativnu veličinu polja
** Korisnik ne može unijeti polje veličine 0

Klikom na „Generiraj“ generiraju se polja ovisno o broju dodanih uzoraka te veličina polja ovisno o unesenim veličinama uzoraka. U polja se ubacuju slučajno generirani cjelobrojni brojevi u rasponu od donje do gornjoj granice. Postotak izvršenosti generiranja prikazuje unutar novog prozora (Slika 5.).

![alt text](https://imgur.com/imM4t0B.jpg "Slika 5.") <br>
Slika 5. Postotak izvršenosti generiranja uzoraka <br>

Klikom na „Testiraj“ izvršava se sortiranje generiranih uzoraka pomoću odabranih algoritama za sortiranje. Postotak izvršenosti sortiranja prikazuje se unutar novo prozora (Slika 6.).

![alt text](https://imgur.com/ygdHAm0.jpg "Slika 6.") <br>
 
Slika 6. Postotak izvršenosti sortiranja uzoraka <br>

Nakon završetka sortiranja otvara se novi prozor koji sadrži stupčasti graf koji prikazuje ovisnosti potrebnog vremena o veličine uzorka za odabrane algoritme, te linijski graf koji prikazuje ovisnost broja usporedbi o veličini uzoraka za odabrane algoritme. Na slici 7. nalazi se prikaz linijskog i stupčastog grafa za 3 uzorka veličine 2000, 4000 i 8000 sortiranih pomoću bubble, insertion i merge sort algoritama.
 
![alt text](https://imgur.com/VOz5GOP.jpg "Slika 6.") <br>
Slika 7. Prikaz rezultat testiranja pomoću stupčastog i linijskog grafa
