package view;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import model.SortingAlgorithm;

public class GraphController implements Initializable{
	@FXML
    private BarChart<String, Double> bcTimeChart;

    @FXML
    private CategoryAxis xName;

    @FXML
    private NumberAxis yTime;
    
    @FXML
    private LineChart<Number, Number> lcCompareChart;

    @FXML
    private NumberAxis xSample;

    @FXML
    private NumberAxis yCompare;
    

    
    @Override
	public void initialize(URL location, ResourceBundle resources) {
    	
	}
	
	public void initData(SortingAlgorithm[] sortingAlgorithm, int[] sizeOfGeneratedSamples) {
		DecimalFormat totalTimeFormat = new DecimalFormat("#.##");

    	for(int i = 0; i < sizeOfGeneratedSamples.length; i++) {
    		XYChart.Series<String, Double> timeSeries = new XYChart.Series<>();  
    		timeSeries.setName(Integer.toString(sizeOfGeneratedSamples[i])); 

	    	for(int j = 0; j < sortingAlgorithm.length; j++) {
	    		System.out.println(sortingAlgorithm[j].getName() + ": " + sortingAlgorithm[j].getTotalTime(i) + "ms");
	    		timeSeries.getData().add(new XYChart.Data<>(sortingAlgorithm[j].getName(), sortingAlgorithm[j].getTotalTime(i))); 	
	    	}
	    	
	    	bcTimeChart.getData().add(timeSeries);
    	}
    	
    	for(int i = 0; i < sortingAlgorithm.length; i++) {
    		XYChart.Series<Number, Number> sampleSeries = new XYChart.Series<>();
    		sampleSeries.setName(sortingAlgorithm[i].getName());
    		
    		for(int j = 0; j < sizeOfGeneratedSamples.length; j++) {
    			System.out.println(sortingAlgorithm[i].getName() + ": Velicina: " + sizeOfGeneratedSamples[j] + " Usporedbe: " + sortingAlgorithm[i].getListOfNumberComparisons(j));
    			sampleSeries.getData().add(new XYChart.Data<>(sizeOfGeneratedSamples[j], sortingAlgorithm[i].getListOfNumberComparisons(j))); 
    		}
    		
    		lcCompareChart.getData().add(sampleSeries);
    	}
		
	}

	
}
