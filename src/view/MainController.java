package view;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.UnaryOperator;

import org.controlsfx.control.CheckComboBox;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import model.BubbleSort;
import model.HeapSort;
import model.InsertionSort;
import model.MergeSort;
import model.QuickSort;
import model.Sample;
import model.SelectionSort;
import model.SortingAlgorithm;

public class MainController implements Initializable{
	private static final int DEFUALT_LOWER_LIMIT = 0;
	private static final int DEFUALT_UPPER_LIMIT = 5000;
	
	private int lowerLimit;
	private int upperLimit;
	private int selectedSampleIndex;
	private int sizeOfSelectedSample;
	
	private ObservableList<String> listOfAlgorithm;
	private ObservableList<String> listOfSelectedAlgorithm;
	
	private ObservableList<Sample> sampleList;
	private UnaryOperator<Change> integerFilter;
	private StringConverter<Integer> converter;
	
	private ArrayList<int[]> generatedSamples = new ArrayList<>();
	private int[] sizeOfGeneratedSamples;
	
	private ArrayList<int[]> insertionSamples = new ArrayList<>();
	private ArrayList<int[]> selectionSamples = new ArrayList<>();
	private ArrayList<int[]> bubbleSamples = new ArrayList<>();
	private ArrayList<int[]> quickSamples = new ArrayList<>();
	private ArrayList<int[]> heapSamples = new ArrayList<>();
	private ArrayList<int[]> mergeSamples = new ArrayList<>();
	
	private SortingAlgorithm[] sortingAlgorithm;
	
	@FXML
	private CheckBox cbActiveLimit;
	@FXML
	private TextField tfLowerLimit;
	@FXML
	private TextField tfUpperLimit;
	@FXML
	private ListView<String> lvAlgorithmList;
	@FXML
	private Label lbLowerLimit;
	@FXML
	private Label lbUpperLimit;
	@FXML
	private Button btnAddSample;
	@FXML
	private Button btnUpdateSample;
	@FXML
	private Button btnDeleteSample;
	@FXML
	private Button btnClearSamples;
	@FXML
	private Button btnGenerateSamples;
	@FXML
	private TextField tfSizeOfSelectedSample;
	@FXML
	private ListView<Sample> lvSamples;
	@FXML
	private Button btnTestGeneratedSamples;
	@FXML
    private CheckComboBox<String> ccbAlgorithmSelect;
	 
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		lowerLimit = DEFUALT_LOWER_LIMIT;
		upperLimit = DEFUALT_UPPER_LIMIT;
		
		selectedSampleIndex = -1;
		sizeOfSelectedSample = 0;
		
		sampleList = FXCollections.observableArrayList();
		listOfSelectedAlgorithm = FXCollections.observableArrayList();
		listOfAlgorithm = FXCollections.observableArrayList();
		listOfAlgorithm.addAll("Selection sort", "Insertion sort", "Merge sort", "Bubble sort", "Quick sort", "Heap sort");
		
		ccbAlgorithmSelect.getItems().addAll(listOfAlgorithm);
		ccbAlgorithmSelect.setTitle("Odabir algoritama...");
		
		ccbAlgorithmSelect.getCheckModel().getCheckedItems().addListener(new ListChangeListener<String>() {
		     public void onChanged(ListChangeListener.Change<? extends String> c) {
		    	 listOfSelectedAlgorithm.clear();
		    	 listOfSelectedAlgorithm.addAll(ccbAlgorithmSelect.getCheckModel().getCheckedItems());
		    	 lvAlgorithmList.setItems(listOfSelectedAlgorithm);
		     }
		 });
		
		try {
			setUpNumberFields();
		} catch(NullPointerException e) {
			System.out.println(e.getMessage());
		}
	}
	
	@FXML
	public void checkLimit(ActionEvent event) {
		if(cbActiveLimit.isSelected()) {
			tfLowerLimit.setVisible(true);
			tfUpperLimit.setVisible(true);
			lbLowerLimit.setVisible(true);
			lbUpperLimit.setVisible(true);
		}
		else {
			tfLowerLimit.setVisible(false);
			tfUpperLimit.setVisible(false);
			lbLowerLimit.setVisible(false);
			lbUpperLimit.setVisible(false);
			lowerLimit = DEFUALT_LOWER_LIMIT;
			upperLimit = DEFUALT_UPPER_LIMIT;
			setUpNumberFields();
		}
	}
	
	private void setUpNumberFields() {
		integerFilter = change -> {
            String newText = change.getControlNewText();
            if (newText.matches("([1-9][0-9]*)?")) { 
                return change;
            
            }
            return null;
        };
        
        converter = new IntegerStringConverter() {
            @Override
            public Integer fromString(String s) {
                if (s.isEmpty()) return 0;
                return super.fromString(s);
            }
        };
        
        TextFormatter<Integer> lowerTextFormatter = new TextFormatter<Integer>(converter, lowerLimit, integerFilter);
        tfLowerLimit.setTextFormatter(lowerTextFormatter);
        
        TextFormatter<Integer> upperTextFormatter = new TextFormatter<Integer>(converter, upperLimit, integerFilter);
        tfUpperLimit.setTextFormatter(upperTextFormatter);
        
        TextFormatter<Integer> sizeOfSelectedSampleTextFormatter = new TextFormatter<Integer>(converter, sizeOfSelectedSample, integerFilter);
        tfSizeOfSelectedSample.setTextFormatter(sizeOfSelectedSampleTextFormatter);
        
	}
	
	@FXML
	public void addSample(ActionEvent event) {
		int sizeOfCurrentSample = 0;
		try {
			sizeOfCurrentSample = Integer.parseInt(tfSizeOfSelectedSample.getText());
		} catch (NumberFormatException error) {
			sizeOfCurrentSample = 0;
		}

		if (sizeOfCurrentSample > 0 && sizeOfCurrentSample <= 1000000) {
			if (sampleList.size() < 5) {
				sampleList.add(new Sample("Uzorak ", sizeOfCurrentSample));
				
				lvSamples.setItems(sampleList);
				
				tfSizeOfSelectedSample.clear();
				
				onSampleListClickListener();
			}
			else {
				errorMsg("Ne mo�ete dodati vi�e od 5 uzoraka.");
			}
		}
		else {
			errorMsg("Veli�ina uzorka ne mo�e biti manja od 1 niti ve�a od 1 000 000.");
		}
	}
	
	private void onSampleListClickListener() {
		lvSamples.setOnMouseClicked(e -> {
			if(!lvSamples.getSelectionModel().isEmpty()) {
				selectedSampleIndex = lvSamples.getSelectionModel().getSelectedIndex();
				sizeOfSelectedSample = lvSamples.getSelectionModel().getSelectedItem().getSize();
	
				TextFormatter<Integer> sizeOfSelectedSampleTextFormatter = new TextFormatter<Integer>(converter, sizeOfSelectedSample, integerFilter);
		        tfSizeOfSelectedSample.setTextFormatter(sizeOfSelectedSampleTextFormatter);
			}
		});
	}

	@FXML
	public void updateSample(ActionEvent event) {
		int sizeOfCurrentSample = 0;
		try {
			sizeOfCurrentSample = Integer.parseInt(tfSizeOfSelectedSample.getText());
		} catch (NumberFormatException error) {
			sizeOfCurrentSample = 0;
		}

		if (selectedSampleIndex != -1 && sizeOfCurrentSample > 0 && sizeOfCurrentSample <= 1000000 && sizeOfSelectedSample != sizeOfCurrentSample) {
			sampleList.remove(selectedSampleIndex);
			sampleList.add(selectedSampleIndex, new Sample("Uzorak ", sizeOfCurrentSample));
			lvSamples.getSelectionModel().select(selectedSampleIndex);
		}
		else {
			errorMsg("Veli�ina uzorka ne mo�e biti manja od 1 niti ve�a od 1 000 000.");
		}
		
	}
	
	@FXML
	public void deleteSample(ActionEvent event) {
		if(selectedSampleIndex != -1) {
			sampleList.remove(selectedSampleIndex);
			lvSamples.getSelectionModel().clearSelection();
			selectedSampleIndex = -1;
			tfSizeOfSelectedSample.clear();
		}
	}
	
	@FXML
	public void clearSamples(ActionEvent event) {
		selectedSampleIndex = -1;
		sampleList.clear();
		tfSizeOfSelectedSample.clear();
	}

	@FXML
	public void generateSamples(ActionEvent event) {
		if (sampleList.size() > 0) {
			try {
				lowerLimit = Integer.parseInt(tfLowerLimit.getText());
				upperLimit = Integer.parseInt(tfUpperLimit.getText());
			} catch (NumberFormatException error) {
				lowerLimit = DEFUALT_LOWER_LIMIT;
				upperLimit = DEFUALT_UPPER_LIMIT;
			}
			
			if (lowerLimit > upperLimit) {
				errorMsg("Donja granica ne mo�e biti ve�a od gornje.");
				return;
			} 
			else if (upperLimit > 10000) {
				errorMsg("Gornja granica ne mo�e biti ve�a od 10 000.");
				return;
			}
			
			ProgressForm progressForm = new ProgressForm("Generiranje uzoraka", btnGenerateSamples);

            Task<Void> generateTask = new Task<Void>() {
                @Override
                public Void call() throws InterruptedException {
                	int count = 0;
                	generatedSamples.clear();
                	sizeOfGeneratedSamples = new int[sampleList.size()];
                	
                	for (Sample sample : sampleList) {
        				int []a = new int[sample.getSize()];
        				int []in = new int[sample.getSize()];
        				int []s = new int[sample.getSize()];
        				int []h = new int[sample.getSize()];
        				int []b = new int[sample.getSize()];
        				int []m = new int[sample.getSize()];
        				int []q = new int[sample.getSize()];
        				
        				for (int i = 0; i < sample.getSize(); i++) {
        					int randNum = ThreadLocalRandom.current().nextInt(lowerLimit, upperLimit + 1);
        					a[i] = randNum;
        					in[i] = randNum;
        					s[i] = randNum;
        					h[i] = randNum;
        					b[i] = randNum;
        					m[i] = randNum;
        					q[i] = randNum;
        					
        				}
        				
        				generatedSamples.add(a);
        				sizeOfGeneratedSamples[count] = sample.getSize();
        				
        				insertionSamples.add(in);
        				selectionSamples.add(s);
        				heapSamples.add(h);
        				bubbleSamples.add(b);
        				quickSamples.add(q);
        				mergeSamples.add(m);
        				
        				count += 1;
        				updateProgress(count, sampleList.size());
    					Thread.sleep(5);
        				
        			}
                    updateProgress(count,count);
                    return null ;
                }
            };
            
            // binds progress of progress bars to progress of task:
            progressForm.activateProgressBar(generateTask);

            // update the UI 
            generateTask.setOnSucceeded(e -> {
            	progressForm.getDialogStage().close();
                btnGenerateSamples.setDisable(false);
            });
            
            btnGenerateSamples.setDisable(true);
            progressForm.getDialogStage().show();

            Thread thread = new Thread(generateTask);
            
            thread.start();
			
		} 
		else {
			errorMsg("Niste dodali uzorke.");
		}
	}

	/*private void putSamplesInGeneratedList() {
		generatedSampleList.clear();
		
		for (int[] a : generatedSamples) {
			String sampleArray = " = [";
			for (int i = 0; i < a.length; i++) {
				if (i != a.length - 1)
					sampleArray += a[i] + ", ";
				else 
					sampleArray += a[i] + "]";
					
			}
			generatedSampleList.add(new Sample("Uzorak ", a.length, sampleArray));
		}
	}*/
	
	@FXML
	public void startTestingGeneratedSamples(ActionEvent event) {
		if(listOfSelectedAlgorithm.size() > 0) {
			if(generatedSamples.size() > 0) {
				if(!generatedSamples.isEmpty()) {
					ProgressForm progressForm = new ProgressForm("Sortiranje uzoraka", btnTestGeneratedSamples);
	
		            Task<Void> sortingTask = new Task<Void>() {
		                @Override
		                public Void call() throws InterruptedException {
		                	sortingAlgorithm = new SortingAlgorithm[listOfSelectedAlgorithm.size()];
		                	int count = 0, indexOfSelectedAlgorithm = -1;

		                	if(ccbAlgorithmSelect.getCheckModel().isChecked("Selection sort")) {
		                		SelectionSort selectionSort = new SelectionSort();
		    					indexOfSelectedAlgorithm += 1;
		    					
		    					for (int[] a : selectionSamples) {
		    						long startTime = System.nanoTime();
		    						selectionSort.sort(a);
		    						long endTime = System.nanoTime();
		    						double totalTime = (endTime - startTime) / 1000000.0;
		    						selectionSort.addTotalTime(totalTime);
		    						totalTime = 0.0;
		    						

		    						count += 1;
			        				updateProgress(count, selectionSamples.size());
			    					Thread.sleep(5);
		    					}
		    					
		    					sortingAlgorithm[indexOfSelectedAlgorithm] = selectionSort;
		    				}
		                	if(ccbAlgorithmSelect.getCheckModel().isChecked("Insertion sort")) {
		                		InsertionSort insertionSort = new InsertionSort();
		                		indexOfSelectedAlgorithm += 1;
		                		
		                		for (int[] a : insertionSamples) {
		                			long startTime = System.nanoTime();
		    						insertionSort.sort(a);
		    						long endTime = System.nanoTime();
		    						insertionSort.addTotalTime((endTime - startTime) / 1000000.0);
		
		    						count += 1;
			        				updateProgress(count, insertionSamples.size());
			    					Thread.sleep(5);
		    					}
		                		
		                		sortingAlgorithm[indexOfSelectedAlgorithm] = insertionSort;
		                	}
		                	
		                	if(ccbAlgorithmSelect.getCheckModel().isChecked("Merge sort")) {
		                		MergeSort mergeSort = new MergeSort();
		                		indexOfSelectedAlgorithm += 1;
		                		
		                		for (int[] a : mergeSamples) {
		                			long startTime = System.nanoTime();
		                			mergeSort.sort(a, 0, a.length - 1);
		                			long endTime = System.nanoTime();
		                			mergeSort.addTotalTime((endTime - startTime) / 1000000.0);
		                			
		    						count += 1;
			        				updateProgress(count, mergeSamples.size());
			    					Thread.sleep(5);
		    					}
		                		
		                		sortingAlgorithm[indexOfSelectedAlgorithm] = mergeSort;
		                	}
		                	
		                	if(ccbAlgorithmSelect.getCheckModel().isChecked("Bubble sort")) {
		                		BubbleSort bubbleSort = new BubbleSort();
		                		indexOfSelectedAlgorithm += 1;
		                		
		                		for (int[] a : bubbleSamples) {
		                			long startTime = System.nanoTime();
		                			bubbleSort.sort(a);
		                			long endTime = System.nanoTime();
		                			bubbleSort.addTotalTime((endTime - startTime) / 1000000.0);
		                			
		    						count += 1;
			        				updateProgress(count, bubbleSamples.size());
			    					Thread.sleep(5);
		    					}
		                		
		                		sortingAlgorithm[indexOfSelectedAlgorithm] = bubbleSort;
		                	}
		                	
		                	if(ccbAlgorithmSelect.getCheckModel().isChecked("Quick sort")) {
		                		QuickSort quickSort = new QuickSort();
		                		indexOfSelectedAlgorithm += 1;
		                		
		                		for (int[] a : quickSamples) {
		                			long startTime = System.nanoTime();
		                			quickSort.sort(a, 0, a.length - 1);
		                			long endTime = System.nanoTime();
		                			quickSort.addTotalTime((endTime - startTime) / 1000000.0);
		                			
		    						count += 1;
			        				updateProgress(count, quickSamples.size());
			    					Thread.sleep(5);
		    					}
		                		
		                		sortingAlgorithm[indexOfSelectedAlgorithm] = quickSort;
		                	}
		                	
		                	if(ccbAlgorithmSelect.getCheckModel().isChecked("Heap sort")) {
		                		HeapSort heapSort = new HeapSort();
		                		indexOfSelectedAlgorithm += 1;
		                		
		                		for (int[] a : heapSamples) {
		                			long startTime = System.nanoTime();
		                			heapSort.sort(a);
		                			long endTime = System.nanoTime();
		                			heapSort.addTotalTime((endTime - startTime) / 1000000.0);
		                			
		    						count += 1;
			        				updateProgress(count, heapSamples.size());
			    					Thread.sleep(5);
		    					}
		                		
		                		sortingAlgorithm[indexOfSelectedAlgorithm] = heapSort;
		                	}
		                	
		                    updateProgress(count,count);
		                    return null ;
		                }
		            };
		            
		            // binds progress of progress bars to progress of task:
		            progressForm.activateProgressBar(sortingTask);
	
		            // update the UI 
		            sortingTask.setOnSucceeded(se -> {
		            	progressForm.getDialogStage().close();
		            	btnTestGeneratedSamples.setDisable(false);
		            	try {
			        		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Graph.fxml"));
			        		Parent root1 = (Parent) loader.load();
			        		Stage graphStage = new Stage(StageStyle.DECORATED);
		
			        		graphStage.setScene(new Scene(root1));
			        		graphStage.setTitle("Vizualni prikaz rezultata testiranja");
			        		//graphStage.setOnHiding(we -> graphStage = null);
		
			        		GraphController graphController = loader.<GraphController>getController();
			        		graphController.initData(sortingAlgorithm, sizeOfGeneratedSamples);

			        		generatedSamples.clear();
			        		graphStage.show();
		        			
		        		} catch (IOException err) {
		        			err.printStackTrace();
		        		}
		            });
		            
		            btnTestGeneratedSamples.setDisable(true);
		            progressForm.getDialogStage().show();
	
		            Thread thread = new Thread(sortingTask);
		            
		            thread.start();
				}
				else {
					errorMsg("Molimo generirajte ponovo uzorke.");
				}
			}
			else {
				errorMsg("Niste generirali uzorke.");
			}
		}
		else {
			errorMsg("Niste odabrali algoritme za testiranje.");
		}
	}
	
	public void errorMsg(String message) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Gre�ka");
		alert.setHeaderText(null);
		alert.setContentText(message);

		alert.showAndWait();
	}
	
	public static class ProgressForm {
        private final Stage dialogStage;
        private final ProgressBar progressBar = new ProgressBar();
        private final ProgressIndicator progressIndicator = new ProgressIndicator();

        public ProgressForm(String dialogTitle, Button btnStart) {
            dialogStage = new Stage();
            dialogStage.initStyle(StageStyle.UTILITY);
            dialogStage.setResizable(false);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.setOnHiding(we -> btnStart.setDisable(false));

            // PROGRESS BAR
            final Label label = new Label();
            label.setText("");

            progressBar.setProgress(-1F);
            progressIndicator.setProgress(-1F);

            final HBox hb = new HBox();
            hb.setSpacing(40);
            hb.setAlignment(Pos.CENTER);
            hb.getChildren().addAll(progressBar, progressIndicator);

            Scene scene = new Scene(hb);
            dialogStage.setScene(scene);
            dialogStage.setTitle(dialogTitle);
        }

        public void activateProgressBar(final Task<?> task)  {
        	progressBar.progressProperty().bind(task.progressProperty());
            progressIndicator.progressProperty().bind(task.progressProperty());
            dialogStage.show();
        }

        public Stage getDialogStage() {
            return dialogStage;
        }
    }

	
	
}
