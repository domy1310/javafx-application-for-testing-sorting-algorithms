package model;

public abstract class SortingAlgorithm {

	public abstract String getName();
	
	public abstract double getTotalTime(int index);
	
	public abstract int getListOfNumberComparisons(int index);
}
