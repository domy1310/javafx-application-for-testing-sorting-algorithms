package model;

import java.util.ArrayList;
import java.util.List;

public class HeapSort extends SortingAlgorithm{
	private String name;
	private List<Double> totalTime;
	private int numberOfComparisons;
	private List<Integer> listOfNumberComparisons; 
		
	public HeapSort() {
		this.totalTime = new ArrayList<Double>();
		this.numberOfComparisons = 0;
		this.name = getClass().getSimpleName().toString();
		this.listOfNumberComparisons = new ArrayList<Integer>();
	}
	
	public void sort(int array[]) { 
	    int n = array.length; 

	    for (int i = n / 2 - 1; i >= 0; i--) {
	        heapify(array, n, i); 
	        numberOfComparisons += 1;
	    }

	    for (int i=n-1; i>=0; i--) { 
	        int temp = array[0]; 
	        array[0] = array[i]; 
	        array[i] = temp; 

	        heapify(array, i, 0); 
	        numberOfComparisons += 1;
	    } 
	} 

	public void heapify(int array[], int n, int i) { 
	    int largest = i; 
	    int l = 2*i + 1; 
	    int r = 2*i + 2; 

	    if (l < n && array[l] > array[largest]) 
	        largest = l; 

	    if (r < n && array[r] > array[largest]) 
	        largest = r; 
	 
	    if (largest != i) { 
	        int swap = array[i]; 
	        array[i] = array[largest]; 
	        array[largest] = swap; 

	        heapify(array, n, largest); 
	    } 
	}

	public String getName() {
		return this.name;
	}

	public double getTotalTime(int index) {
		return this.totalTime.get(index);
	}
	
	public void addTotalTime(double totalTime) {
		this.totalTime.add(totalTime);
		this.listOfNumberComparisons.add(getNumberOfComparisons());
		this.numberOfComparisons = 0;
	}
	
	private int getNumberOfComparisons() {
		return this.numberOfComparisons;
	}
	
	public int getListOfNumberComparisons(int index) {
		return this.listOfNumberComparisons.get(index);
	}
}
