package model;

import java.util.ArrayList;
import java.util.List;

public class SelectionSort extends SortingAlgorithm{
	private String name;
	private List<Double> totalTime;
	private int numberOfComparisons;
	private List<Integer> listOfNumberComparisons; 
		
	public SelectionSort() {
		this.totalTime = new ArrayList<Double>();
		this.numberOfComparisons = 0;
		this.name = getClass().getSimpleName().toString();
		this.listOfNumberComparisons = new ArrayList<Integer>();
	}
	
	public void sort(int[] array){
	    for (int i = 0; i < array.length - 1; i++) {
	        int index = i;
	        for (int j = i + 1; j < array.length; j++) {
	            if (array[j] < array[index]) {
	                index = j;
	            }
	            numberOfComparisons += 1;
	        }
			int temp = array[index];
			array[index] = array[i];
			array[i] = temp;
	    }

	    //addTotalTime((double)(endTime - startTime) / 1_000_000_000.0);
	}
	
	public String getName() {
		return this.name;
	}

	public double getTotalTime(int index) {
		return this.totalTime.get(index);
	}
	
	public void addTotalTime(double totalTime) {
		this.totalTime.add(totalTime);
		this.listOfNumberComparisons.add(getNumberOfComparisons());
		this.numberOfComparisons = 0;
	}
	
	private int getNumberOfComparisons() {
		return this.numberOfComparisons;
	}
	
	public int getListOfNumberComparisons(int index) {
		return this.listOfNumberComparisons.get(index);
	}
}






