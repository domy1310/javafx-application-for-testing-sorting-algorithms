package model;

import java.util.ArrayList;
import java.util.List;

public class MergeSort extends SortingAlgorithm{
	private String name;
	private List<Double> totalTime;
	private int numberOfComparisons;
	private List<Integer> listOfNumberComparisons; 
		
	public MergeSort() {
		this.totalTime = new ArrayList<Double>();
		this.numberOfComparisons = 0;
		this.name = getClass().getSimpleName().toString();
		this.listOfNumberComparisons = new ArrayList<Integer>();
	}
	
	public void merge(int array[], int left, int mid, int right) { 
	    int i, j, k; 
	    int n1 = mid - left + 1; 
	    int n2 =  right - mid; 

	    int L[] = new int[n1], R[] = new int[n2]; 

	    for (i = 0; i < n1; i++) 
	        L[i] = array[left + i]; 
	    for (j = 0; j < n2; j++) 
	        R[j] = array[mid + 1+ j]; 
	  
	    i = 0; 
	    j = 0; 
	    k = left; 
	    while (i < n1 && j < n2) { 
	        if (L[i] <= R[j]) { 
	        	array[k] = L[i]; 
	            i++; 
	        } 
	        else { 
	        	array[k] = R[j]; 
	            j++; 
	        } 
	        k++; 
	        numberOfComparisons += 1;
	    } 
	  
	    while (i < n1) { 
	    	array[k] = L[i]; 
	        i++; 
	        k++; 
	        numberOfComparisons += 1;
	    } 
	  
	    while (j < n2) { 
	    	array[k] = R[j]; 
	        j++; 
	        k++; 
	        numberOfComparisons += 1;
	    } 
	} 
	  
	public void sort(int array[], int left, int right) { 
	    if (left < right) { 
	        int mid = left+(right-left)/2; 

	        sort(array, left, mid); 
	        sort(array, mid+1, right); 
	  
	        merge(array, left, mid, right); 
	    }
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public double getTotalTime(int index) {
		return this.totalTime.get(index);
	}
	
	public void addTotalTime(double totalTime) {
		this.totalTime.add(totalTime);
		this.listOfNumberComparisons.add(getNumberOfComparisons());
		this.numberOfComparisons = 0;
	}
	
	private int getNumberOfComparisons() {
		return this.numberOfComparisons;
	}
	
	public int getListOfNumberComparisons(int index) {
		return this.listOfNumberComparisons.get(index);
	}
}
