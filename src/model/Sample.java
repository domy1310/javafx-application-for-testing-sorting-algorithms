package model;

public class Sample {
	private String name;
	private int size;
	private String array;
	
	public Sample(String name, int size) {
		this.name = name;
		this.size = size;
		this.array = "";
	}
	
	public Sample(String name, int size, String array) {
		this.name = name;
		this.size = size;
		this.array = array;
	}

	public int getSize() {
		return size;
	}
	
	public String getName() {
		return name;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
	public String getArray() {
		return array;
	}
	
	@Override
	public String toString() {
		return name + " [" + Integer.toString(getSize()) + "]" + array;
	}
	
}
