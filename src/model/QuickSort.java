package model;

import java.util.ArrayList;
import java.util.List;

public class QuickSort extends SortingAlgorithm {
	private String name;
	private List<Double> totalTime;
	private int numberOfComparisons;
	private List<Integer> listOfNumberComparisons; 
		
	public QuickSort() {
		this.totalTime = new ArrayList<Double>();
		this.numberOfComparisons = 0;
		this.name = getClass().getSimpleName().toString();
		this.listOfNumberComparisons = new ArrayList<Integer>();
	}
	
	public int partition(int array[], int low, int high) { 
	    int pivot = array[high];  
	    int i = (low-1);
	    for (int j=low; j<high; j++) { 
	        if (array[j] <= pivot) { 
	            i++; 

	            int temp = array[i]; 
	            array[i] = array[j]; 
	            array[j] = temp; 
	        } 
	        numberOfComparisons += 1;
	    } 

	    int temp = array[i+1]; 
	    array[i+1] = array[high]; 
	    array[high] = temp; 
	  
	    return i+1; 
	} 
	  
	public void sort(int array[], int low, int high) { 
	    if (low < high) { 
	        int pi = partition(array, low, high); 

	        sort(array, low, pi-1); 
	        sort(array, pi+1, high); 
	    } 
	}

	public String getName() {
		return this.name;
	}

	public double getTotalTime(int index) {
		return this.totalTime.get(index);
	}
	
	public void addTotalTime(double totalTime) {
		this.totalTime.add(totalTime);
		this.listOfNumberComparisons.add(getNumberOfComparisons());
		this.numberOfComparisons = 0;
	}
	
	private int getNumberOfComparisons() {
		return this.numberOfComparisons;
	}
	
	public int getListOfNumberComparisons(int index) {
		return this.listOfNumberComparisons.get(index);
	}
}
