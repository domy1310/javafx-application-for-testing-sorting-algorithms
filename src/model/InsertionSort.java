package model;

import java.util.ArrayList;
import java.util.List;

public class InsertionSort extends SortingAlgorithm{
	private String name;
	private List<Double> totalTime;
	private int numberOfComparisons;
	private List<Integer> listOfNumberComparisons; 
		
	public InsertionSort() {
		this.totalTime = new ArrayList<Double>();
		this.numberOfComparisons = 0;
		this.name = getClass().getSimpleName().toString();
		this.listOfNumberComparisons = new ArrayList<Integer>();
	}
	
	public void sort(int[] array) { 
	    int n = array.length; 
	    
	    for (int i = 1; i < n; ++i) { 
	        int key = array[i]; 
	        int j = i - 1; 
	        while (j >= 0 && array[j] > key) { 
	            array[j + 1] = array[j]; 
	            j = j - 1; 
	            numberOfComparisons += 1;
	        } 
	        array[j + 1] = key; 
	    } 
	}
	
	public String getName() {
		return this.name;
	}

	public double getTotalTime(int index) {
		return this.totalTime.get(index);
	}
	
	public void addTotalTime(double totalTime) {
		this.totalTime.add(totalTime);
		this.listOfNumberComparisons.add(getNumberOfComparisons());
		this.numberOfComparisons = 0;
	}
	
	private int getNumberOfComparisons() {
		return this.numberOfComparisons;
	}
	
	public int getListOfNumberComparisons(int index) {
		return this.listOfNumberComparisons.get(index);
	}
}
