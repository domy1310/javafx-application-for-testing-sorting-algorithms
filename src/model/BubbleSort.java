package model;

import java.util.ArrayList;
import java.util.List;

public class BubbleSort extends SortingAlgorithm{
	private String name;
	private int numberOfComparisons;
	private List<Double> totalTime;
	private List<Integer> listOfNumberComparisons; 
		
	public BubbleSort() {
		this.totalTime = new ArrayList<Double>();
		this.numberOfComparisons = 0;
		this.name = getClass().getSimpleName().toString();
		this.listOfNumberComparisons = new ArrayList<Integer>();
	}
	
	public void sort(int array[]) { 
	    boolean swapped; 
	    
	    for (int i = 0; i < array.length - 1; i++) { 
	        swapped = false; 
	        for (int j = 0; j < array.length - i - 1; j++) { 
	            if (array[j] > array[j + 1]) { 
	                int temp = array[j]; 
	                array[j] = array[j + 1]; 
	                array[j + 1] = temp; 
	                swapped = true; 
	            } 
	            numberOfComparisons += 1;
	        } 
	        if (swapped == false) 
	            break; 
	    } 
	}
	
	public String getName() {
		return this.name;
	}

	public double getTotalTime(int index) {
		return this.totalTime.get(index);
	}
	
	public void addTotalTime(double totalTime) {
		this.totalTime.add(totalTime);
		this.listOfNumberComparisons.add(getNumberOfComparisons());
		this.numberOfComparisons = 0;
	}
	
	private int getNumberOfComparisons() {
		return this.numberOfComparisons;
	}
	
	public int getListOfNumberComparisons(int index) {
		return this.listOfNumberComparisons.get(index);
	}
}
